use crate::rgtab::*;
use crate::weechatbuffer::*;
use crate::weechatcommand::*;
use crate::weechatevent::*;
use crate::weechatmessage::*;
use gtk::{glib::source, glib::IOCondition, prelude::*};
use std::cell::RefCell;
use std::collections::HashMap;
use std::io::{ErrorKind, Write};
use std::os::unix::{io::IntoRawFd, net::UnixStream};
use std::{env, error, fs, io, path::PathBuf, process::Command, rc::Rc, thread, time};

pub type MessageHandler = fn(
    exec: Rc<RefCell<WeechatExec>>,
    message: &mut WeechatMessage,
) -> Result<(), Box<dyn error::Error>>;
// TODO: I think we need a trait here that the GUI provides an implementation of
pub type StartDoneHandler =
    Box<dyn Fn(Rc<RefCell<WeechatExec>>) -> Result<(), Box<dyn error::Error>>>;
// TODO: .. and that trait should hide the RGTab
pub type TabHandler = Box<
    dyn Fn(
        Rc<RefCell<WeechatExec>>,
        String,
        String,
        Rc<RefCell<WeechatBuffer>>,
    ) -> Result<Rc<RefCell<RGTab>>, Box<dyn error::Error>>,
>;

pub struct WeechatExec {
    sock: UnixStream,
    commandcallbacks: HashMap<String, MessageHandler>,
    eventcallbacks: HashMap<String, MessageHandler>,
    pub buffers: HashMap<String, Rc<RefCell<WeechatBuffer>>>,
    start_done_handler: Option<StartDoneHandler>,
    pub open_tab: Option<TabHandler>,
    tagsequence: u64,
}

impl WeechatExec {
    pub fn submit(
        &mut self,
        tag: String,
        text: String,
        handler: Option<MessageHandler>,
    ) -> Result<(), Box<dyn error::Error>> {
        let numbered_tag = format!("{}{}", tag, self.tagsequence);
        self.tagsequence += 1;
        if let Some(some_handler) = handler {
            self.commandcallbacks
                .insert(numbered_tag.clone(), some_handler);
        }
        write!(self.sock, "({}) {}", numbered_tag, text)?;

        Ok(())
    }

    pub fn dispatch(
        exec: Rc<RefCell<WeechatExec>>,
        message: WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        let mut message = message;
        match message.id.chars().nth(0) {
            None => Err(Box::new(io::Error::new(ErrorKind::Other, "Empty id field"))),
            Some('_') => {
                let mut res = Ok(());
                // Reserved weechat id, so must be event
                // TODO: These can happen multiple times, so don't want to remove
                // but can't hold the borrow on rrexec so can't keep the hashmap
                // result alive; so take out and put back - must be better way!
                let opt_handler = exec.clone().borrow_mut().eventcallbacks.remove(&message.id);

                if let Some(handler) = opt_handler {
                    res = handler(exec.clone(), &mut message);
                    exec.clone()
                        .borrow_mut()
                        .eventcallbacks
                        .insert(message.id, handler);
                } else {
                    res = Err(Box::new(io::Error::new(
                        ErrorKind::Other,
                        format!("Unknown event id '{}'", message.id),
                    )));
                }
                res
            }
            Some(_) => {
                // Response to a command
                // command callbacks only get called once, so remove as we process
                let opt_handler = exec
                    .clone()
                    .borrow_mut()
                    .commandcallbacks
                    .remove(&message.id);
                if let Some(handler) = opt_handler {
                    handler(exec, &mut message)
                } else {
                    Err(Box::new(io::Error::new(
                        ErrorKind::Other,
                        format!("Unknown response id '{}'", message.id),
                    )))
                }
            }
        }
    }

    // Call back from the "hdata buffer..." command - we should get a list of buffers
    // which at this stage is the core.weechat and also one for the relay.list which we
    // don't care about.
    fn cb_hdatabuffer(
        exec: Rc<RefCell<WeechatExec>>,
        message: &mut WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        let wmo = message.objects.pop().expect("No object");
        let hdata = wmo.get_hdata()?;
        let full_name_index = hdata.get_key_index("full_name".to_string(), WCMTYPESTR)?;

        for o in hdata.objects {
            let name: &str = o.values[full_name_index]
                .get_string()?
                .as_ref()
                .expect("No name"); // Note Option
            let buffer_ptr = &o.ppath[0]; // Should be buffer pointer
            if name == "core.weechat" {
                WeechatExec::addbuffer(exec.clone(), buffer_ptr.to_string(), name.to_string())?;
            }
        }

        // This should tell us about all changes from here on in - note no response
        exec.borrow_mut()
            .submit("sync".to_string(), "sync\n".to_string(), None)?;

        let sdh = exec.borrow_mut().start_done_handler.take();
        if let Some(handler) = sdh {
            handler(exec)
        } else {
            Ok(())
        }
    }

    // Call back from the "info version" command response we do just after init
    fn cb_infoinit(
        exec: Rc<RefCell<WeechatExec>>,
        _message: &mut WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        //println!("cb_infoinit {:?}", message);
        WeechatCommand::submit_hdata(
            &mut exec.borrow_mut(),
            "buffer".to_string(),
            "gui_buffers(*)".to_string(),
            "full_name".to_string(),
            WeechatExec::cb_hdatabuffer,
        )
    }

    // Call back from the "handshake" command response
    fn cb_handshake(
        exec: Rc<RefCell<WeechatExec>>,
        message: &mut WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        println!("cb_handshake {:?}", message);
        assert_eq!(message.objects.len(), 1);
        if let Some(MessageObject::HashTable(_ht)) = message.objects.pop() {
            // Init doesn't have a response [Oh great]
            exec.borrow_mut()
                .submit("init".to_string(), "init password=\n".to_string(), None)?;
            exec.borrow_mut().submit(
                "initinfo".to_string(),
                "info version\n".to_string(),
                Some(WeechatExec::cb_infoinit),
            )?
        } else {
            panic!("handshake response missing hashtable")
        }
        Ok(())
    }

    pub fn start(start_done: StartDoneHandler, open_tab: TabHandler) {
        let runtime_dir = env::var("XDG_RUNTIME_DIR").expect("Unset XDG_RUNTIME_DIR");
        let mut runtime_path = PathBuf::from(runtime_dir);
        runtime_path.push("weechat.socket"); // should probably make this configurable

        // Clean up any existing socket
        // It's fine for this to fail
        let _ = fs::remove_file(runtime_path.clone());

        let runtime_path_str = runtime_path.to_str().expect("Bad socket path");

        println!("Starting weechat...");
        // TODO: Make all the paths and args configurable
        let _weechat = Command::new("weechat-headless")
            .arg("-a") // no connect
            .arg("-r") // Run command
            // TODO!!!! FIX!!! weechat doesn't seem to differentiate auth on unix socket
            // from network which is a pain
            .arg("/set relay.network.allow_empty_password on")
            .arg("/relay add unix.weechat ".to_owned() + runtime_path_str)
            .arg("--stdout")
            .spawn()
            .expect("Failed to start weechat");

        // We have no good way to know when weechat is ready except to watch for
        // the socket to exist
        println!("Waiting for weechat socket...");
        while fs::metadata(runtime_path_str).is_err() {
            println!(".");
            thread::sleep(time::Duration::from_millis(100));
        }

        let mut sock =
            UnixStream::connect(runtime_path_str).expect("Failed to open weechat socket");
        let read_sock = sock
            .try_clone()
            .expect("Couldn't clone weechat socket (read)");

        let state = Rc::new(RefCell::new(WeechatExec {
            sock: sock
                .try_clone()
                .expect("Couldn't clone weechat socket (state)"),
            commandcallbacks: HashMap::from([]),
            eventcallbacks: HashMap::from(EVENTHANDLERS.map(|(s, h)| (s.to_string(), h))),
            buffers: HashMap::from([]),
            start_done_handler: Some(start_done),
            open_tab: Some(open_tab),
            tagsequence: 0,
        }));

        let state_for_fd = state.clone();
        source::unix_fd_add_local(
            read_sock.into_raw_fd(),
            IOCondition::IN,
            move |_fd, condition| {
                if condition.contains(IOCondition::HUP) {
                    // TODO: Clean up somehow
                    println!("Got HUP");
                    return Continue(false);
                }
                let message = WeechatMessage::read(&mut sock).expect("Reading message");
                println!("socket read cond: {:?} message: {:#?}", condition, message);
                WeechatExec::dispatch(state_for_fd.clone(), message).expect("Failed to dispatch");
                Continue(true)
            },
        );
        state
            .borrow_mut()
            .submit(
                "handshake".to_string(),
                "handshake password_hash_algo=plain,compression=off\n".to_string(),
                Some(WeechatExec::cb_handshake),
            )
            .expect("handshake write");
    }
}
