use std::error;

use crate::weechatexec::*;

// A representation of commands from the client to weechat
// Based on: https://weechat.org/files/doc/stable/weechat_relay_protocol.en.html#commands

pub struct WeechatCommand {}

impl WeechatCommand {
    // Submit an hdata command for hdata:ptrvars with the optional keys
    pub fn submit_hdata(
        exec: &mut WeechatExec,
        hdata: String,
        ptrvars: String,
        keys: String,
        handler: MessageHandler,
    ) -> Result<(), Box<dyn error::Error>> {
        exec.submit(
            "hdata".to_string(),
            format!("hdata {}:{} {}\n", hdata, ptrvars, keys),
            Some(handler),
        )
    }

    // The input command submits data into a buffer (like typing), it has no direct
    // response message.
    // Note: There's no mechanism to escape the input
    // TODO: Error if there's a newline in the data
    // TODO: probably should take some type of buffer object
    pub fn submit_input(
        exec: &mut WeechatExec,
        buffer: String,
        data: String,
    ) -> Result<(), Box<dyn error::Error>> {
        exec.submit(
            "input".to_string(),
            format!("input {} {}\n", buffer, data),
            None,
        )
    }
}
