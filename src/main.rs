use gtk::{prelude::*, Application};
use std::{cell::RefCell, error::Error, rc::Rc};

mod rgtab;
mod rgwin;
mod weechatbuffer;
mod weechatcommand;
mod weechatevent;
mod weechatexec;
mod weechatmessage;

use self::rgtab::*;
use self::rgwin::*;
use self::weechatbuffer::*;
use self::weechatcommand::*;
use self::weechatexec::*;

fn input_handler_core_weechat(exec: &mut WeechatExec, rgt: &RGTab, text: String) {
    println!("Entry: '{}' for '{}'", text, rgt.name);
    WeechatCommand::submit_input(exec, rgt.name.clone(), text).expect("submit_input");
}

fn start_complete(_rrexec: Rc<RefCell<WeechatExec>>) {
    println!("Start done!");
}

fn add_tab(
    rrrgwin: Rc<RefCell<RGWin>>,
    rrexec: Rc<RefCell<WeechatExec>>,
    _ptr: String,
    name: String,
    rrbuf: Rc<RefCell<WeechatBuffer>>,
) -> Result<Rc<RefCell<RGTab>>, Box<dyn Error>> {
    let exec = rrexec.clone();
    rrrgwin.borrow_mut().add_tab(
        name,
        Some(rrbuf.clone()),
        Box::new(
            // TODO: Choose input handler
            move |rgt, text| input_handler_core_weechat(&mut exec.borrow_mut(), rgt, text),
        ),
    )
}

fn start(app: &Application) {
    let rgwin = RGWin::create(&app, "Win 1".to_string()).unwrap();
    let rrrgwin = Rc::new(RefCell::new(rgwin));
    WeechatExec::start(
        Box::new(move |e| {
            start_complete(e);
            Ok(())
        }),
        Box::new(
            move |rre, ptr, name, rrbuffer| -> Result<Rc<RefCell<RGTab>>, Box<dyn Error>> {
                add_tab(rrrgwin.clone(), rre, ptr, name, rrbuffer)
            },
        ),
    );
}

fn main() {
    // Started with the gtk4-rs book example
    let app = Application::builder()
        .application_id("org.treblig.reechat")
        .build();

    app.connect_activate(start);

    app.run();
}
