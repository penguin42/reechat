use gtk::{
    gio::Menu, prelude::*, Application, ApplicationWindow, Label, Notebook, Orientation,
    PopoverMenuBar, PositionType,
};
use std::error::Error;
use std::{cell::RefCell, rc::Rc};

use crate::rgtab::*;
use crate::weechatbuffer::*;

pub type InputHandler = Box<dyn Fn(&RGTab, String)>;
pub struct RGWin {
    name: String,
    win: ApplicationWindow,
    notebook: Notebook,
}

fn build_appmenu() -> Menu {
    let m = Menu::new();
    m.append_submenu(Some("Sub"), &Menu::new());
    m.freeze();
    m
}

impl RGWin {
    pub fn create(app: &Application, name: String) -> Result<RGWin, Box<dyn Error>> {
        // Menu bar at the top
        let pmb = PopoverMenuBar::from_model(Some(&build_appmenu()));
        // Under that a Notebook with tabs
        let notebook = Notebook::new();
        notebook.set_tab_pos(PositionType::Bottom);
        notebook.set_show_tabs(true);
        notebook.set_scrollable(true); // the tab area

        // window->vbox and it has the main parts in
        let topbox = gtk::Box::new(Orientation::Vertical, 0);
        topbox.append(&pmb);
        topbox.append(&notebook);

        let win = ApplicationWindow::builder()
            .application(app)
            .title(&name)
            .child(&topbox)
            .build();

        win.present();

        let rwin = RGWin {
            win,
            notebook,
            name,
        };

        Ok(rwin)
    }
    pub fn add_tab(
        &mut self,
        name: String,
        buffer: Option<Rc<RefCell<WeechatBuffer>>>,
        input_handler: InputHandler,
    ) -> Result<Rc<RefCell<RGTab>>, Box<dyn Error>> {
        let res = RGTab::new(name.clone(), buffer, input_handler)?;
        let tablabel = Label::new(Some(&name));
        let _pagenum = self
            .notebook
            .append_page(&res.borrow().vbox, Some(&tablabel)); // Todo check for error

        Ok(res)
    }
}
