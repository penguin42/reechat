use byteorder::{BigEndian, ReadBytesExt};
use std::io::{ErrorKind, Read};
use std::{error, io};

// A representation of messages from weechat to the client (us)
// as defined on https://weechat.org/files/doc/stable/weechat_relay_protocol.en.html#messages
//
// The 3 character type of an object
type TypeTriplet = [char; 3];

pub const WCMTYPEHTB: TypeTriplet = ['h', 't', 'b'];
pub const WCMTYPEINT: TypeTriplet = ['i', 'n', 't'];
pub const WCMTYPEPTR: TypeTriplet = ['p', 't', 'r'];
pub const WCMTYPESTR: TypeTriplet = ['s', 't', 'r'];

#[derive(Debug)]
pub enum MessageObject {
    Char(u8),
    Integer(i32),
    LongInteger(String),
    String(Option<String>), // Option to allow Null
    Buffer(Vec<u8>),
    Pointer(String), // Hex - should we use a specific string type?
    Time(String),    // decimal - should we actually convert to integer?
    HashTable(WMOHashTable),
    Hdata(WMOHdata),
    Info((String, String)),
    Array(WMOArray),
    // ToDo: infolist, array
}

#[derive(Debug)]
pub struct WMOArray {
    typet: TypeTriplet,
    entries: Vec<MessageObject>,
}

#[derive(Debug)]
pub struct WMOHdataObject {
    pub ppath: Vec<String>, // pointers indexed by the hpath
    pub values: Vec<MessageObject>,
}

#[derive(Debug)]
pub struct WMOHdata {
    pub hpath: Vec<String>,
    keys: Vec<(String, TypeTriplet)>,
    pub objects: Vec<WMOHdataObject>,
}

impl WMOHdata {
    // Find the index of a given named key, and check the type
    // The index is then an index into the values field of a WMOHdataObject
    pub fn get_key_index(
        &self,
        expected_name: String,
        expected_type: TypeTriplet,
    ) -> Result<usize, Box<dyn error::Error>> {
        let mut count = 0;
        for (name, typetriplet) in &self.keys {
            if *name == expected_name {
                if *typetriplet == expected_type {
                    return Ok(count);
                } else {
                    return Err(Box::new(io::Error::new(
                        ErrorKind::Other,
                        format!(
                            "Expected type: '{:?}' found '{:?}'",
                            expected_type, typetriplet
                        ),
                    )));
                }
            }
            count = count + 1;
        }
        Err(Box::new(io::Error::new(
            ErrorKind::Other,
            format!("Key {} not found", expected_name),
        )))
    }
}

#[derive(Debug)]
pub struct WMOHashTable {
    type_keys: TypeTriplet,
    type_values: TypeTriplet,
    entries: Vec<(MessageObject, MessageObject)>, // Probably actually want a hash table!
}

impl MessageObject {
    // Helpers to check the message object is as expected
    // TODO: How to make these less copied?
    // TODO: This one needs to be ref
    pub fn get_hdata(self) -> Result<WMOHdata, Box<dyn error::Error>> {
        if let MessageObject::Hdata(result) = self {
            return Ok(result);
        } else {
            return Err(Box::new(io::Error::new(
                ErrorKind::Other,
                format!("Unexpected Message type, expecting Hdata: {:#?}", self),
            )));
        }
    }

    pub fn get_pointer(&self) -> Result<&String, Box<dyn error::Error>> {
        if let MessageObject::Pointer(result) = self {
            return Ok(result);
        } else {
            return Err(Box::new(io::Error::new(
                ErrorKind::Other,
                format!("Unexpected Message type, expecting Pointer: {:#?}", self),
            )));
        }
    }

    // Note strings are Option, so maybe None
    pub fn get_string(&self) -> Result<&Option<String>, Box<dyn error::Error>> {
        if let MessageObject::String(result) = self {
            return Ok(result);
        } else {
            return Err(Box::new(io::Error::new(
                ErrorKind::Other,
                format!("Unexpected Message type, expecting String: {:#?}", self),
            )));
        }
    }
}

#[derive(Debug)]
pub struct WeechatMessage {
    pub id: String,
    pub objects: Vec<MessageObject>,
}

impl WeechatMessage {
    // Helper for the simple case of a single named object
    pub fn get_single_named(
        &mut self,
        expected_id: String,
    ) -> Result<MessageObject, Box<dyn error::Error>> {
        if self.id != expected_id {
            return Err(Box::new(io::Error::new(
                ErrorKind::Other,
                format!("Unexpected id '{}', expecting '{}'", self.id, expected_id),
            )));
        }

        if self.objects.len() != 1 {
            return Err(Box::new(io::Error::new(
                ErrorKind::Other,
                format!("Was expecting 1 object, but have {}", self.objects.len()),
            )));
        }

        Ok(self.objects.pop().expect("No entry"))
    }
}

// Read the 3 character type code
fn read_type(stream: &mut dyn Read) -> Result<TypeTriplet, Box<dyn error::Error>> {
    let mut typecode: TypeTriplet = ['0'; 3];
    let mut u8code: [u8; 3] = [0; 3];
    if let Err(e) = stream.read_exact(&mut u8code) {
        return Err(Box::new(e));
    };
    typecode[0] = u8code[0] as char;
    typecode[1] = u8code[1] as char;
    typecode[2] = u8code[2] as char;
    Ok(typecode)
}

// Read an object from the stream given the type
fn read_object(
    stream: &mut dyn Read,
    tt: TypeTriplet,
) -> Result<MessageObject, Box<dyn error::Error>> {
    match tt {
        ['a', 'r', 'r'] => return read_message_array(stream),
        ['c', 'h', 'r'] => return read_message_char(stream),
        ['h', 'd', 'a'] => return read_message_hda(stream),
        WCMTYPEHTB => return read_message_htb(stream),
        ['i', 'n', 'f'] => return read_message_info(stream),
        WCMTYPEINT => return read_message_int(stream),
        WCMTYPEPTR => return read_message_pointer(stream),
        WCMTYPESTR => return read_message_string(stream),
        ['t', 'i', 'm'] => return read_message_time(stream),
        // TODO: Other types
        _ => {
            return Err(Box::new(io::Error::new(
                ErrorKind::Other,
                format!("Unexpected typecode: {:#?}", tt),
            )))
        }
    }
}

// An array is a type code, a count and then a set of count entries of that type
fn read_message_array(stream: &mut dyn Read) -> Result<MessageObject, Box<dyn error::Error>> {
    let typecode = read_type(stream)?;
    let count = stream.read_u32::<BigEndian>()?;
    let mut entries: Vec<MessageObject> = vec![];

    for _c in 0..count {
        let obj = read_object(stream, typecode)?;
        entries.push(obj);
    }

    let result = {
        WMOArray {
            typet: typecode,
            entries: entries,
        }
    };
    Ok(MessageObject::Array(result))
}

// Read an 'hdata'
fn read_message_hda(stream: &mut dyn Read) -> Result<MessageObject, Box<dyn error::Error>> {
    let mut result = WMOHdata {
        hpath: vec![],
        keys: vec![],
        objects: vec![],
    };
    // the 'h-path' a / separated list of path elements; can be Null
    let h_path = read_message_string(stream)?;

    if let MessageObject::String(Some(str)) = h_path {
        result.hpath = str.split('/').map(|s| s.to_string()).collect();
    }

    // the 'keys' list, a comma separated list of name:type, can be null
    let keys = read_message_string(stream)?;
    if let MessageObject::String(Some(str)) = keys {
        let mut keys: Vec<(String, TypeTriplet)> = vec![];
        for keyutf in str.split(',') {
            let key: Vec<char> = keyutf.chars().collect();
            // Each should be name:type where type is the 3 character code
            let len = key.len();
            if len < 5 {
                return Err(Box::new(io::Error::new(
                    ErrorKind::Other,
                    format!("Bad hda key: {}", keyutf),
                )));
            }
            if key[len - 4] != ':' {
                return Err(Box::new(io::Error::new(
                    ErrorKind::Other,
                    format!("Bad hda key, expecting : found {}", key[len - 4]),
                )));
            }
            let mut keytype: TypeTriplet = ['0'; 3];
            keytype[0] = key[len - 3];
            keytype[1] = key[len - 2];
            keytype[2] = key[len - 1];
            match keyutf.get(0..len - 4) {
                None => {
                    return Err(Box::new(io::Error::new(
                        ErrorKind::Other,
                        format!("Bad hda key (sub): {}", keyutf),
                    )))
                }

                Some(keyname) => keys.push((keyname.to_string(), keytype)),
            }
        }
        result.keys = keys;
    }

    // The number of 'sets of objects'
    let count = stream.read_u32::<BigEndian>()?;

    let hpath_len = result.hpath.len();
    let values_len = result.keys.len();
    for _c in 0..count {
        // First the p-path, i.e. one pointer for each entry in hpath
        let mut p_path: Vec<String> = vec![];

        for _p in 0..hpath_len {
            let p = read_message_pointer(stream)?;

            if let MessageObject::Pointer(ptr) = p {
                p_path.push(ptr);
            }
        }

        // Now the values
        let mut values: Vec<MessageObject> = vec![];
        for v in 0..values_len {
            let obj = read_object(stream, result.keys[v].1)?;
            values.push(obj);
        }
        let object = {
            WMOHdataObject {
                ppath: p_path,
                values: values,
            }
        };
        result.objects.push(object);
    }

    Ok(MessageObject::Hdata(result))
}

// Read a hash table
fn read_message_htb(stream: &mut dyn Read) -> Result<MessageObject, Box<dyn error::Error>> {
    // All the entries in the ht are the same type, but we have to read what the key/value is
    let type_keys = read_type(stream)?;
    let type_values = read_type(stream)?;

    // Then the number of items
    let count = stream.read_u32::<BigEndian>()?;

    let mut result = WMOHashTable {
        type_keys,
        type_values,
        entries: vec![],
    };
    for _ in 0..count {
        let key = read_object(stream, type_keys)?;
        let value = read_object(stream, type_values)?;

        result.entries.push((key, value));
    }
    Ok(MessageObject::HashTable(result))
}

// Read an 'info' - i.e. a string (name) and string (value)
fn read_message_info(stream: &mut dyn Read) -> Result<MessageObject, Box<dyn error::Error>> {
    let name = read_message_string(stream)?;
    let value = read_message_string(stream)?;

    Ok(MessageObject::Info((
        if let MessageObject::String(Some(real_name)) = name {
            real_name
        } else {
            panic!("Bad name in info message");
        },
        if let MessageObject::String(Some(real_value)) = value {
            real_value
        } else {
            panic!("Bad value in info message");
        },
    )))
}

fn read_message_int(stream: &mut dyn Read) -> Result<MessageObject, Box<dyn error::Error>> {
    let value = stream.read_i32::<BigEndian>()?;

    Ok(MessageObject::Integer(value))
}

// Read a string
fn read_message_string(stream: &mut dyn Read) -> Result<MessageObject, Box<dyn error::Error>> {
    let length = stream.read_u32::<BigEndian>()?;

    // For some weird reason Weechat allows representation of a Null pointer
    if length == 0xffffffff {
        return Ok(MessageObject::String(None));
    }

    let mut result: String = "".to_string();
    stream.take(length as u64).read_to_string(&mut result)?;

    return Ok(MessageObject::String(Some(result)));
}

// Read a pointer - i.e a string in hex with one byte length count
fn read_message_pointer(stream: &mut dyn Read) -> Result<MessageObject, Box<dyn error::Error>> {
    let length = stream.read_u8()?;

    let mut result: String = "".to_string();
    stream.take(length as u64).read_to_string(&mut result)?;

    if let Some(_) = result.find(|c: char| !c.is_ascii_hexdigit()) {
        return Err(Box::new(io::Error::new(
            ErrorKind::Other,
            format!("Non hex in pointer {}", result),
        )));
    }
    return Ok(MessageObject::Pointer(result));
}

// Read a time - i.e a string in decimal with one byte length count
fn read_message_time(stream: &mut dyn Read) -> Result<MessageObject, Box<dyn error::Error>> {
    let length = stream.read_u8()?;

    let mut result: String = "".to_string();
    stream.take(length as u64).read_to_string(&mut result)?;

    if let Some(_) = result.find(|c: char| !c.is_ascii_digit()) {
        return Err(Box::new(io::Error::new(
            ErrorKind::Other,
            format!("Non digit in time {}", result),
        )));
    }
    return Ok(MessageObject::Time(result));
}

// Read a char - i.e. a u8
fn read_message_char(stream: &mut dyn Read) -> Result<MessageObject, Box<dyn error::Error>> {
    let result = stream.read_u8()?;
    return Ok(MessageObject::Char(result));
}

impl WeechatMessage {
    fn read_id(&mut self, stream: &mut dyn Read) -> Result<(), Box<dyn error::Error>> {
        // Now the id string, 4 byte length + data (0 is valid)
        let idlen = stream.read_u32::<BigEndian>()?;
        stream.take(idlen as u64).read_to_string(&mut self.id)?;

        Ok(())
    }

    pub fn read(stream: &mut dyn Read) -> Result<WeechatMessage, Box<dyn error::Error>> {
        // We start with a 4 byte big endian integer length
        let len = stream.read_u32::<BigEndian>()?;
        // Len includes the header it is part of
        if len < 4 {
            return Err(Box::new(io::Error::new(
                ErrorKind::Other,
                "Len too small for itself",
            )));
        }

        let mut taken = stream.take((len - 4) as u64);

        // The next byte is a compression type that should be 0 for uncompressed
        let compression = taken.read_u8()?;
        if compression != 0 {
            return Err(Box::new(io::Error::new(
                ErrorKind::Other,
                "Unexpectedly compressed stream",
            )));
        }

        let mut result = WeechatMessage {
            id: "".to_string(),
            objects: vec![],
        };

        result.read_id(&mut taken)?;
        // loop over objects
        loop {
            let reresult = read_type(&mut taken);
            if let Err(e) = reresult {
                // That's fine, just end of stream
                if let Some(ioe) = (*e).downcast_ref::<std::io::Error>() {
                    if ioe.kind() == ErrorKind::UnexpectedEof {
                        break;
                    }
                }
                // Some other error
                return Err(e);
            }
            if let Ok(typecode) = reresult {
                let obj = read_object(&mut taken, typecode)?;
                result.objects.push(obj);
            }
        }
        Ok(result)
    }
}
