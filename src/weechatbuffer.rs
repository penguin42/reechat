use std::cell::RefCell;
use std::{error, rc::Rc};

use crate::rgtab::*;
use crate::weechatexec::*;

pub struct WeechatBuffer {
    ptr: String,
    name: String,
    tab: Option<Rc<RefCell<RGTab>>>, // TODO: abstract type, may be a list?
}

impl WeechatBuffer {
    pub fn insertmessage(
        &self,
        prefix: String,
        message: String,
    ) -> Result<(), Box<dyn error::Error>> {
        if let Some(rrrgtab) = &self.tab {
            rrrgtab.borrow_mut().insertmessage(prefix, message)?
        }
        Ok(())
    }

    pub fn rename(&mut self, new_name: String) -> Result<(), Box<dyn error::Error>> {
        self.name = new_name;
        if let Some(rrrgtab) = &self.tab {
            rrrgtab.borrow_mut().rename(self.name.clone())?
        }
        Ok(())
    }
}

impl WeechatExec {
    pub fn addbuffer(
        rrexec: Rc<RefCell<WeechatExec>>,
        ptr: String,
        name: String,
    ) -> Result<(), Box<dyn error::Error>> {
        let buf = WeechatBuffer {
            ptr: ptr.clone(),
            name: name.clone(),
            tab: None,
        };

        let rrbuf = Rc::new(RefCell::new(buf));

        rrexec
            .borrow_mut()
            .buffers
            .insert(ptr.clone(), rrbuf.clone());

        let rrexecbor = rrexec.borrow();
        let th = rrexecbor.open_tab.as_ref().unwrap();
        let rgtab = th(rrexec.clone(), ptr, name, rrbuf.clone())?;
        rrbuf.borrow_mut().tab = Some(rgtab);

        Ok(())
    }

    pub fn getbuffer(&self, ptr: String) -> Option<Rc<RefCell<WeechatBuffer>>> {
        match self.buffers.get(&ptr) {
            Some(x) => Some(x.clone()),
            None => None,
        }
    }
}
