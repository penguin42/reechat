use gtk::{prelude::*, Entry, Label, ListBox, Orientation, ScrolledWindow, TextMark, TextView};
use std::error::Error;
use std::{cell::RefCell, rc::Rc};

use crate::rgwin::InputHandler;
use crate::weechatbuffer::*;

pub struct RGTab {
    pub name: String,
    pub buffer: Option<Rc<RefCell<WeechatBuffer>>>,
    pub vbox: gtk::Box,
    pub entry: Entry,
    pub tablabel: Label,
    pub textview: TextView,
    endmark: TextMark, // Always at end of TextView buffer
}

impl RGTab {
    pub fn new(
        name: String,
        buffer: Option<Rc<RefCell<WeechatBuffer>>>,
        input_handler: InputHandler,
    ) -> Result<Rc<RefCell<RGTab>>, Box<dyn Error>> {
        let tabvbox = gtk::Box::new(Orientation::Vertical, 0);

        let topiclabel = Label::new(Some("Topic text"));
        tabvbox.append(&topiclabel);

        // A horizontal box with the conversation and list of participants
        let tabmidbox = gtk::Box::new(Orientation::Horizontal, 0);
        let endmark = TextMark::new(None, false /* Right gravity */);
        let textview = TextView::new();
        textview.set_hexpand(true);
        textview.set_vexpand(true);
        textview.set_editable(false);
        textview
            .buffer()
            .add_mark(&endmark, &textview.buffer().end_iter());

        let textviewscroller = ScrolledWindow::builder().child(&textview).build();

        tabmidbox.append(&textviewscroller);
        let listbox = ListBox::new();
        listbox.set_vexpand(true);

        listbox.append(&Label::new(Some("Joshua")));
        listbox.append(&Label::new(Some("David")));
        listbox.append(&Label::new(Some("Jennifer")));
        listbox.append(&Label::new(Some("Stephen")));
        tabmidbox.append(&listbox);

        tabvbox.append(&tabmidbox);

        let entry = Entry::new(); // Might want something multiline
        let tablabel = Label::new(Some(&name));
        tabvbox.append(&entry);

        let rrtab = Rc::new(RefCell::new(RGTab {
            name: name.clone(),
            buffer: buffer.clone(),
            entry,
            textview,
            tablabel,
            vbox: tabvbox,
            endmark,
        }));
        let rtab_for_entry = rrtab.clone();

        rrtab.borrow().entry.connect_activate(move |e| {
            input_handler(&rtab_for_entry.borrow(), e.text().to_string());
            e.set_text("")
        });
        Ok(rrtab)
    }
    pub fn insertmessage(&self, prefix: String, message: String) -> Result<(), Box<dyn Error>> {
        let buffer = self.textview.buffer();
        let mut toadd = prefix;
        let mut where_to_insert = buffer.end_iter();
        toadd += ":";
        toadd += &message;
        toadd += "\n\r";
        buffer.insert(&mut where_to_insert, &toadd);
        self.textview
            .scroll_to_mark(&self.endmark, 0.0, true, 0.0, 0.0);
        Ok(())
    }
    pub fn rename(&mut self, new_name: String) -> Result<(), Box<dyn Error>> {
        self.name = new_name;
        self.tablabel.set_label(&self.name);

        Ok(())
    }
}
