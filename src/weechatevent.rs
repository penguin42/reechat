use crate::weechatexec::*;
use crate::weechatmessage::*;
use std::{cell::RefCell, error, io, rc::Rc};

// This file handles reserved events defined at https://weechat.org/files/doc/stable/weechat_relay_protocol.en.html#message_identifier
// They've already been protocol decoded by weechatmessage.rs
//
pub const EVENTHANDLERS: [(&str, MessageHandler); 9] = [
    ("_buffer_line_added", WeechatEvent::buffer_line_added),
    ("_buffer_localvar_added", WeechatEvent::buffer_localvar_added),
    ("_buffer_localvar_changed", WeechatEvent::buffer_localvar_changed),
    ("_buffer_merged", WeechatEvent::buffer_merged),
    ("_buffer_opened", WeechatEvent::buffer_opened),
    ("_buffer_renamed", WeechatEvent::buffer_renamed),
    ("_buffer_title_changed", WeechatEvent::buffer_title_changed),
    ("_nicklist", WeechatEvent::nicklist),
    ("_nicklist_diff", WeechatEvent::nicklist_diff),
];

pub struct WeechatEvent {}

impl WeechatEvent {
    fn buffer_line_added(
        exec: Rc<RefCell<WeechatExec>>,
        message: &mut WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        let hdata = (*message)
            .get_single_named("_buffer_line_added".to_string())?
            .get_hdata()?;
        let buffer_index = hdata.get_key_index("buffer".to_string(), WCMTYPEPTR)?;
        let prefix_index = hdata.get_key_index("prefix".to_string(), WCMTYPESTR)?;
        let message_index = hdata.get_key_index("message".to_string(), WCMTYPESTR)?;

        // Each object seems to be one line, I've only ever seen a single line
        // per event, but lets walk them
        for o in hdata.objects {
            let buffer = o.values[buffer_index].get_pointer()?;
            let prefix = o.values[prefix_index].get_string()?; // Note are Options
            let message = o.values[message_index].get_string()?;

            let wcbo = exec.borrow().getbuffer(buffer.clone());
            if let Some(wcb) = wcbo {
                wcb.borrow_mut().insertmessage(
                    prefix.clone().unwrap_or("".to_string()),
                    message.clone().unwrap_or("".to_string()),
                )?
            } else {
                println!("Missing buffer: {:?}\n", buffer);
            }
        }
        Ok(())
    }


    // as far as I can tell, thes buffer_localvar_added has an htb of
    // all the localvars, not just the ne one
    fn buffer_localvar_added(
        exec: Rc<RefCell<WeechatExec>>,
        message: &mut WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        let hdata = (*message)
            .get_single_named("_buffer_localvar_added".to_string())?
            .get_hdata()?;
        let _lv_index = hdata.get_key_index("local_variables".to_string(), WCMTYPEHTB)?;
        if hdata.hpath.len() != 1 || hdata.hpath[0] != "buffer" {
            return Err(Box::new(io::Error::new(
                io::ErrorKind::Other,
                format!("Bad hpath in 'buffer_localvar_added' event"),
            )));
        }
        // TODO: Localvars not implemented yet
        Ok(())
    }

    fn buffer_localvar_changed(
        exec: Rc<RefCell<WeechatExec>>,
        message: &mut WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        let hdata = (*message)
            .get_single_named("_buffer_localvar_changed".to_string())?
            .get_hdata()?;
        let _lv_index = hdata.get_key_index("local_variables".to_string(), WCMTYPEHTB)?;
        if hdata.hpath.len() != 1 || hdata.hpath[0] != "buffer" {
            return Err(Box::new(io::Error::new(
                io::ErrorKind::Other,
                format!("Bad hpath in 'buffer_localvar_changed' event"),
            )));
        }
        // TODO: Localvars not implemented yet
        Ok(())
    }

    fn buffer_merged(
        exec: Rc<RefCell<WeechatExec>>,
        message: &mut WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        let hdata = (*message)
            .get_single_named("_buffer_merged".to_string())?
            .get_hdata()?;
        if hdata.hpath.len() != 1 || hdata.hpath[0] != "buffer" {
            return Err(Box::new(io::Error::new(
                io::ErrorKind::Other,
                format!("Bad hpath in 'buffer_merged' event"),
            )));
        }
        // TODO: merging not implemented yet - dont understand it!
        Ok(())
    }

    fn buffer_opened(
        exec: Rc<RefCell<WeechatExec>>,
        message: &mut WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        let hdata = (*message)
            .get_single_named("_buffer_opened".to_string())?
            .get_hdata()?;
        let fullname_index = hdata.get_key_index("full_name".to_string(), WCMTYPESTR)?;
        if hdata.hpath.len() != 1 || hdata.hpath[0] != "buffer" {
            return Err(Box::new(io::Error::new(
                io::ErrorKind::Other,
                format!("Bad hpath in 'buffer_opened' event"),
            )));
        }
        // TODO

        for o in hdata.objects {
            let buffer_ptr = o.ppath[0].clone();
            let full_name_opt = o.values[fullname_index].get_string()?; // Options
            if let Some(full_name) = full_name_opt {
                WeechatExec::addbuffer(exec.clone(), buffer_ptr, full_name.to_string())?
            }
        }
        Ok(())
    }

    // I've seen this happen even just for the shortname being changed
    fn buffer_renamed(
        exec: Rc<RefCell<WeechatExec>>,
        message: &mut WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        let hdata = (*message)
            .get_single_named("_buffer_renamed".to_string())?
            .get_hdata()?;
        let fullname_index = hdata.get_key_index("full_name".to_string(), WCMTYPESTR)?;

        if hdata.hpath.len() != 1 || hdata.hpath[0] != "buffer" {
            return Err(Box::new(io::Error::new(
                io::ErrorKind::Other,
                format!("Bad hpath in 'buffer_renamed' event"),
            )));
        }
        for o in hdata.objects {
            let buffer_ptr = o.ppath[0].clone();
            let full_name_opt = o.values[fullname_index].get_string()?; // Options
            // TODO capture the short name
            if let Some(full_name) = full_name_opt {
                // TODO do the rename
                let wcbo = exec.borrow().getbuffer(buffer_ptr.clone());
                if let Some(wcb) = wcbo {
                    wcb.borrow_mut().rename(full_name.to_string())?
                } else {
                    println!("Missing buffer in buffer_rename: {:?}\n", buffer_ptr);
                }
            }
            // TODO capture the local variables
        }
        Ok(())
    }

    fn buffer_title_changed(
        exec: Rc<RefCell<WeechatExec>>,
        message: &mut WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        let hdata = (*message)
            .get_single_named("_buffer_title_changed".to_string())?
            .get_hdata()?;
        let title_index = hdata.get_key_index("title".to_string(), WCMTYPESTR)?;

        if hdata.hpath.len() != 1 || hdata.hpath[0] != "buffer" {
            return Err(Box::new(io::Error::new(
                io::ErrorKind::Other,
                format!("Bad hpath in 'buffer_title_changed' event"),
            )));
        }
        for o in hdata.objects {
            let buffer_ptr = o.ppath[0].clone();
            let title_opt = o.values[title_index].get_string()?; // Options
            if let Some(title) = title_opt {
                let wcbo = exec.borrow().getbuffer(buffer_ptr.clone());
                if let Some(wcb) = wcbo {
                    // TODO: Actually set the title
                } else {
                    println!("Missing buffer in buffer_title_changed: {:?}\n", buffer_ptr);
                }
            }
        }
        Ok(())
    }

    // This receives a full nicklist
    fn nicklist(
        exec: Rc<RefCell<WeechatExec>>,
        message: &mut WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        let hdata = (*message)
            .get_single_named("_nicklist".to_string())?
            .get_hdata()?;
        if hdata.hpath.len() != 2
            || hdata.hpath[0] != "buffer"
            || hdata.hpath[1] != "nicklist_item" {
            return Err(Box::new(io::Error::new(
                io::ErrorKind::Other,
                format!("Bad hpath in 'nicklist' event"),
            )));
        }
        let _name_index = hdata.get_key_index("name".to_string(), WCMTYPESTR)?;

        // TODO: Nicklists not implemented yet
        Ok(())
    }

    fn nicklist_diff(
        exec: Rc<RefCell<WeechatExec>>,
        message: &mut WeechatMessage,
    ) -> Result<(), Box<dyn error::Error>> {
        let hdata = (*message)
            .get_single_named("_nicklist_diff".to_string())?
            .get_hdata()?;
        if hdata.hpath.len() != 2
            || hdata.hpath[0] != "buffer"
            || hdata.hpath[1] != "nicklist_item" {
            return Err(Box::new(io::Error::new(
                io::ErrorKind::Other,
                format!("Bad hpath in 'nicklist_diff' event"),
            )));
        }
        let _name_index = hdata.get_key_index("name".to_string(), WCMTYPESTR)?;

        // TODO: Nicklists not implemented yet
        Ok(())
    }
}
